const mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);

const Currency = mongoose.Types.Currency;

const Schema = mongoose.Schema;

const ordenesSchema= new Schema({
    codigoProducto:{
        type:String,
        required:true
    },
    nombreGenerico:{
        type:String,
        required:true
    },
    codigoCaja:{
        type:String,
        required:true
    },
    cantidad:{
        type:Number,
        required:true
    },
    precioCompra:{
        type:Currency,
        required:true
    },
    subtotal:{
        type:Currency
    },
    metodoImportacion:{
        type:String,
        required:true
    },
    limitedeMeses:{
        type:Number,
        required:true,
        default:0
    },fechadeVencimiento:{
        type:Date,
        required:true,
    },
})

const comrpaSchema= new Schema({
    fechaCompra:{
        type:Date,
        default:Date.now
    },
    total:{
        type:Currency  
    },
    Activa:{
        type:Boolean,
        default:true
    },
    ordenes:[ordenesSchema]
},{
    timestamps:true
})
