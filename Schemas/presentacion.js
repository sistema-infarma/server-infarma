const mongoose= require('mongoose');

const Schema= mongoose.Schema;

const presentacionSchema= new Schema({
    tipo:{
        type:String,
        required:true,
        unique:true
    }
});

var Presentacion= mongoose.model('Presentacion',presentacionSchema);
module.exports=Presentacion;