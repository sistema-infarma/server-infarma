const mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);

const Currency = mongoose.Types.Currency;

const Schema = mongoose.Schema;

const productoSchema= new Schema({
    nombreComercial:{
        type:String,
        required:true,
        unique:true
    },
    descripcion:{
        type:String,
        required:true,
    },
    activo:{
        type:Boolean,
        default:true
    }
},{
    timestamps:true
})

var Producto = mongoose.model('Producto',productoSchema);
module.exports= Producto;