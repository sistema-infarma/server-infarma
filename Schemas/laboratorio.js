const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const laboratorioSchema= new Schema({
    nombreLaboratorio:{
        type:String,
        unique:true,
        required:true
    },
    numeroTelefono:{
        type:String
    },
    correoLaboratorio:{
        type:String
    },
    activo:{
        type:Boolean,
        default:true
    }
})

const Laboratorio = mongoose.model('Laboratorio',laboratorioSchema);

module.exports= Laboratorio;