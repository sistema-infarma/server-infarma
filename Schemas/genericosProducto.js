const mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);

const Currency = mongoose.Types.Currency;

const Schema = mongoose.Schema;

const detallesPresentacion= new Schema({
    codigoCaja:{
        type:String,
        required:true,
        unique: false,
        sparse: true
    },
    codigoProductoPresentacion:{
        type:String,
        unique:true,
        sparse: true
    },
    fechadeIngreso:{
        type:Date,
        default:Date.now
    },
    fechadeVencimiento:{
        type:Date,
        required:true,
        default:Date.now
    },
    tipo:{
        type:Schema.Types.ObjectId,
        ref:'Presentacion',
        required:true,
        sparse: true
    },
    cantidad:{
        type:Number,
        required:true,
        default:0
    },
    preciodeCompra:{
        type:Currency,
        required:true,
        default:0
    },
    preciodeVenta:{
        type:Currency,
        required:true,
        default:0
    },
    limitedeMeses:{
        type:Number,
        required:true,
        default:0
    },
    metodoImportacion:{
        type:String,
        required:true,
        default:"Barco"
    },
    laboratorio:{
        type:mongoose.Types.ObjectId,
        ref:'Laboratorio',
        required:true,
        sparse: true
    },
    activo:{
        type:Boolean,
        default:true
    }
},{
    timestamps:true
})

const genericoSchema= new Schema({
    producto:{
        type: Schema.Types.ObjectId,
        ref:'Producto',
        required:true
    },
    codigoProducto:{
        type:String,
        unique:true
    },
    nombreGenerico:{
        type:String,
        required:true,
    },activo:{
        type:Boolean,
        default:true
    },
    detalles:[detallesPresentacion]
},{
    timestamps:true
})

const Generico= mongoose.model('Generico',genericoSchema);

module.exports=Generico;