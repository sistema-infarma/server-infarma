const mongoose =require('mongoose')

require('mongoose-currency').loadType(mongoose);

const Producto = require('./producto');
const Presentacion =require('./presentacion')
const Currency = mongoose.Types.Currency;

const Schema = mongoose.Schema;

const ordenesSchema=new Schema({
    producto:{
        type:mongoose.Schema.Types.ObjectId,
        required:true
    },
    tipo:{
        type:String,
        required:true
    },
    cantidad:{
        type:Number,
        required:true
    },
    subtotal:{
        type:Number
    }
})

const ventasSchema= new Schema({
    trabajador:{
        type:String,
        required:true
    },
    ordenes:[ordenesSchema],
    total:{
        type:Number
    }
},{
    timestamps:true
})


ventasSchema.post('save',venta=>{
    
    if(venta.total){
        console.log(venta.ordenes)
        var newproducto=venta.ordenes.map(orden=>{
            if(orden.subtotal){
                return Producto.findById(orden.producto).then(producto=>{
                    return Presentacion.findOne({tipo:orden.tipo}).then(presentacion=>{
                        index=producto.presentacion.map(p=>p.presentacion).indexOf(presentacion._id);
                        if(index!== -1){
                            producto.presentacion[index].cantidad=producto.presentacion[index].cantidad-orden.cantidad;
                            return producto
                        }
                    })
                })
            }  
        })
        Promise.all(newproducto).then(producto=>{
            producto.forEach(producto=>{
                Producto.findByIdAndUpdate(producto._id,{$set:{presentacion:producto.presentacion}},{new:true}).then(producto=>{
                    console.log(producto);
                }).catch(e=>console.log(e))
            })  
        })
    }
})
const Ventas = mongoose.model('Ventas',ventasSchema)
module.exports=Ventas