const passport =require('passport');
const LocalStrategy=require('passport-local').Strategy;
const jwtStrategy= require('passport-jwt').Strategy;
const ExtractJwt= require('passport-jwt').ExtractJwt;
const jwt =require('jsonwebtoken');

const User = require('./Schemas/usuario');
const config= require('./config')
exports.local= passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

exports.getToken=(user)=>{
    return jwt.sign(user,config.secretKey,{expiresIn:1000*60*60*12});
}

const opts={
    jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey:config.secretKey
}

exports.jwtPassport= passport.use(new jwtStrategy(opts,
    (jwt_payload,done)=>{
        User.findOne({_id:jwt_payload._id},(err,user)=>{
            if(err){
                return done(err,false)
            }else if(user){
                return done(null,user)
            }else{
                return done(null,false);
            }
        })
    }));

exports.verifyUser = passport.authenticate('jwt',{session:false});

exports.verifyAdmin=(req,res,next)=>{
    if(req.user.admin){
        next();
    }else{
        err= new Error('You are not authorized to do this operation');
        err.status=403;
        return next(err);
    }
}