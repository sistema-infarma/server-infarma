const Generico= require('./Schemas/genericosProducto');
const Producto=require('./Schemas/producto');
const Presentacion=require('./Schemas/presentacion');


const ABC=['A','B','C','D','E','F','G','H','I','J',"K","L","M","N","Ñ","O","P","Q",
"R","S","T","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

exports.Codificar=(dato1,dato2,count,producto)=>{
    let L1;
    let L2;
    let L3;
    let L1i;
    let L2i;
    let L2ir;
    let L1ir;
    let Difi;
    L1ir=ABC.indexOf(dato1.charAt(0))-Math.floor(Math.random()*4);
    L2ir=ABC.indexOf(dato2.charAt(0))-Math.floor(Math.random()*4);
    if(L1ir<0){

        L1i=32+L1ir;

    }else{
        L1i=L1ir
    }
    if(L2ir<0){
        L2i=32+L2ir;
    }else{
        L2i=L2ir
    }

    L1=ABC[L1i];
    L2=ABC[L2i];

    let countString= count.toString();

    const time=Math.trunc((Date.now()/count));
    const timeString=time.toString();

    const timesubstracted= timeString.substring(timeString.length - 3,timeString.length);
    if(count>=100){
        countString= countString.substring(time.length - 3,time.length);
    }else if(count<100 && count>=10){
        countString= '0'+countString;
    }else if(count<10){
        countString= '00'+countString;
    }
    if(producto){
        if(ABC.indexOf(L1)<ABC.indexOf(L2)){
            Difi=ABC.indexOf(L2)-ABC.indexOf(L1)
        }else{
            Difi=ABC.indexOf(L1)-ABC.indexOf(L2)
        }
        L3=ABC[Difi];
        
        return`${L1}${L2}${L3}-${countString}${timesubstracted}`;
    }
    return`${L1}${L2}-${countString}${timesubstracted}`;
}

exports.PutDetalles= (generico,detalle,presentacion,req,res,next)=>{
    //Asignacion de valores nuevos
    Object.assign(generico.detalles[generico.detalles.findIndex(d=> d._id=detalle._id)],{
        //cantidad
        cantidad:req.body.cantidad?req.body.cantidad:detalle.cantidad,
        //preciodeCompra
        preciodeCompra:req.body.preciodeCompra?req.body.preciodeCompra:detalle.preciodeCompra,
        //preciodeVenta
        preciodeVenta:req.body.preciodeVenta?req.body.preciodeVenta:detalle.preciodeVenta,
        //limitedeMeses
        limitedeMeses:req.body.limitedeMeses?req.body.limitedeMeses:detalle.limitedeMeses,
        //metodoImportacion
        metodoImportacion:req.body.metodoImportacion?req.body.metodoImportacion:detalle.metodoImportacion,
        //tipo
        tipo:req.body.tipo?presentacion._id:detalle.tipo,
        //activo
        activo:!req.body.activo?false:true,
        //Laboratorio
        laboratorio:req.body.laboratorio?req.body.laboratorio:detalle.laboratorio,
        //Codigo
        codigoProductoPresentacion:req.body.tipo?detalle.codigoProductoPresentacion.substring(0,detalle.codigoProductoPresentacion.length-2)+req.body.tipo.substring(0,2).toUpperCase():detalle.codigoProductoPresentacion
    })

    this.saveGenerico(generico,req,res,next);

}

exports.saveGenerico=(generico,req,res,next)=>{
    generico.save().then(generico=>{
        Generico.findById(generico._id).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
            
            res.statusCode=200;
            res.setHeader('Content_Type', 'application/Json');
            res.json(generico); 
        }).catch(e=>next(e));
    }).catch(e=>next(e))
}

exports.createProducto=(req,res,next)=>{
    Producto.findOne({nombreComercial:req.body.nombreComercial}).then(producto=>{
        //Verifico si el producto no existe

            if(!producto){
                Producto.create(req.body).then(producto=>{
                    //Creo el generico del producto
                    if(req.body.nombreGenerico){
                        
                            Generico.create({producto:producto._id,nombreGenerico:req.body.nombreGenerico}).then(generico=>{
                                //Obtengo la cuenta de documtentos en generico para comenzar la codificación
                                Generico.countDocuments({}, (err, count)=> {
                                    //una vez establecida la cuenta procedo a crear el codigo
                                    var codigo= this.Codificar(req.body.nombreComercial,req.body.nombreGenerico,count,true);
                                    //seteo el codigo en el generico
                                    generico.codigoProducto=codigo
                                    //guardo  el generico
                                    generico.save().then(generico=>{
                                        //verifico si se incluyeron detalles para el generico
                                        if(req.body.detalles){
                                            Generico.findOne({"detalles.codigoCaja":req.body.codigoCaja}).then(genericot=>{
                                                console.log(genericot)
                                                if(genericot===null){
                                                    Presentacion.findOne({tipo:req.body.detalles.tipo}).then(presentacion=>{
                                                        if(presentacion){
                                                            //Seteo la referencia para la presentacion
                                                            req.body.detalles.tipo=presentacion._id
                                                            //Establesco el codigo para la presentacion del generico
                                                            req.body.detalles.codigoProductoPresentacion=codigo+presentacion.tipo.substring(0,2).toUpperCase();
                                                            //Introduzco la presentacion del generico
                                                            generico.detalles.push(req.body.detalles);
                                                            //Guardo el generico    
                                                            this.saveGenerico(generico,req,res,next);
                                                        }else{
                                                            res.statusCode=403;
                                                            res.setHeader('Content_Type', 'application/Json');
                                                            res.send("Ingrese una presentacion valida"); 
                                                        }
                                                    }).catch(e=>next(e))
                                                    
                                                }else{
                                                    res.statusCode=403;
                                                    res.setHeader('Content_Type', 'application/Json');
                                                    res.send("El codigo de caja tiene que ser unico"); 
                                                }
                                            }).catch(e=>next(e))
                                        }else{
                                            Generico.findById(generico._id).populate('Producto').populate('detalles.tipo').populate('detalles.laboratorio').then(producto=>{
                                                res.statusCode=200;
                                                res.setHeader('Content_Type', 'application/Json');
                                                res.json(producto); 
                                            })
                                        }
                                    })
                                })
                                
                            })
                    }else{
                        res.statusCode=200;
                        res.setHeader('Content_Type', 'application/Json');
                        res.json(producto); 
                    }
                }).catch(e=>next(e))
            }else{
                res.statusCode=400;
                res.setHeader('Content_Type', 'application/Json');
                res.send('Ese producto ya existe, si desea añadir un nueva version generica o una presentacion al generico de un producto vaya a sus formularios respectivos'); 
            }
    })
}