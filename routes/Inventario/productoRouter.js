const express= require('express');
const bodyParser= require('body-parser');

const mongoose = require('mongoose');

const Producto= require('../../Schemas/producto');
const Generico=require('../../Schemas/genericosProducto');
const authenticate= require('../../authenticate');
const Presentacion= require('../../Schemas/presentacion');
const cors =require('../cors');
const ObjectId=mongoose.Types.ObjectId;
const productoRouter= express.Router();
const genericoRouter=require('./genericosRouter')
const {Codificar, saveGenerico,createProducto}= require('../../helper');
const { count } = require('../../Schemas/producto');

productoRouter.use(bodyParser.json());
productoRouter.use('/genericos',genericoRouter);
productoRouter.route('/')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})

.get(cors.cors,(req,res,next)=>{
        Producto.find(req.query).then(productos=>{
            res.statusCode=200;
            res.setHeader('Content_Type', 'application/Json');
            res.json(productos);
   
        }).catch(e=>next(e))
    
})

.post(cors.corsWithOptions,(req,res,next)=>{
    
    createProducto(req,res,next);
})
.put(cors.corsWithOptions,(res,req,next)=>{
    res.statusCode=403;
    res.end('Put operation not supported on productos');
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.setHeader('Content_Type', 'application/Json');
    res.json({err:"No puede elminiar todos lo productos"});
})

//Specific Product

productoRouter.route('/:productoId')
.options(cors.corsWithOptions,authenticate.verifyUser,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    Producto.findById(req.params.productoId).then(producto=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(producto);
    })
})
.post(cors.corsWithOptions,(req,res)=>{
    res.statusCode=403;
    res.end('post operation not supported on productoId');
})
.put(cors.corsWithOptions,(req,res,next)=>{

   if(req.body.nombreComercial){
        Producto.findById(req.params.productoId).then(producto1=>{
            Producto.findOne({nombreComercial:req.body.nombreComercial}).then(producto2=>{
                if(producto2? producto1._id.equals(producto2._id):true){
                        Producto.findByIdAndUpdate(req.params.productoId,{'$set':req.body},{new:true}).then(producto=>{
                            if(req.body.activo){
                                Generico.findOne({producto:req.params.productoId}).then(generico=>{
                                    generico.activo=req.body.activo
                                    generico.detalles.forEach(detalle=>{
                                        detalle.activo=req.body.activo;
                                    })
                                    saveGenerico(Generico,generico,req,res,next);
                                })
                            }else{
                                Generico.findOne({producto:req.params.productoId}).populate('producto').populate('detalles.tipo').then(generico=>{
                                    res.statusCode=200;
                                    res.setHeader('Content_Type','application/Json');
                                    res.json(generico);
                                })
                            }
                            
                        }).catch(e=>next(e))
                }else{
                    res.statusCode=403;
                    res.send("Intenta modificar el nombre de este producto con uno ya registrado");
                }
            }).catch(e=>next(e))
        }).catch(e=>next(e))
   }else{
        Producto.findByIdAndUpdate(req.params.productoId,{'$set':req.body},{new:true}).then(producto=>{
            if(req.body.activo){
                Generico.findOne({producto:req.params.productoId}).then(generico=>{
                    generico.activo=req.body.activo
                    generico.detalles.forEach(detalle=>{
                        detalle.activo=req.body.activo;
                    })
                    saveGenerico(Generico,generico,req,res,next);
                })
            }else{
                Generico.findOne({producto:req.params.productoId}).populate('producto').populate('detalles.tipo').then(generico=>{
                    res.statusCode=200;
                    res.setHeader('Content_Type','application/Json');
                    res.json(generico);
                })
            }
            
        }).catch(e=>next(e))
   }
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Producto.findByIdAndUpdate(req.params.productoId,{'$set':{activo:false}}).then(producto=>{
        Generico.findOne({producto:req.params.productoId}).then(generico=>{
            generico.detalles.forEach(detalle=>{
                detalle.activo=false;
            })
            saveGenerico(Generico,generico,req,res,next);
        })
    })
})
module.exports =productoRouter;