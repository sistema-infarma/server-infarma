const express= require('express');
const bodyParser= require('body-parser');

const mongoose = require('mongoose');

const Producto= require('../../Schemas/producto');
const Generico=require('../../Schemas/genericosProducto');
const authenticate= require('../../authenticate');
const Presentacion= require('../../Schemas/presentacion');
const {Codificar,PutDetalles, saveGenerico}= require('../../helper');
const cors =require('../cors');
const { populate } = require('../../Schemas/producto');
const genericoRouter= express.Router();

genericoRouter.route('/')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})
.get(cors.corsWithOptions,(req,res,next)=>{
 
        Generico.find(req.query).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(productos=>{
            productos.forEach(producto=>{
                producto.detalles=producto.detalles.filter(detalle=>detalle.activo=true)
            })
            res.statusCode=200;
            res.setHeader('Content_Type', 'application/Json');
            res.json(productos);
        }).catch(e=>next(e));
})
.post(cors.corsWithOptions,(req,res,next)=>{

    Producto.findById(req.body.producto).then(producto=>{
        console.log(producto);
        console.log(req.body)
        if(producto){
            Generico.findOne({nombreGenerico:req.body.nombreGenerico}).then(generico=>{
                if(!generico){
                    Generico.create({producto:producto._id,nombreGenerico:req.body.nombreGenerico}).then(generico=>{
                        Generico.countDocuments({}, (err, count)=> {
                            var codigo= Codificar(producto.nombreComercial,req.body.nombreGenerico,count,true);
                        generico.codigoProducto=codigo

                        generico.save().then(generico=>{
                                console.log(Object.keys(req.body.detalles).length)
                                if(req.body.detalles && Object.keys(req.body.detalles).length !== 0){
                                    Generico.findOne({"detalles.codigoCaja":req.body.codigoCaja}).then(gener=>{
                                        if(!gener){
                                            Presentacion.findOne({tipo:req.body.detalles.tipo}).then(presentacion=>{
                                                if(presentacion){
                                                    console.log(req.body.detalles)
                                                    req.body.detalles.tipo=presentacion._id
                                                    //Establesco el codigo para la presentacion del generico
                                                    req.body.detalles.codigoProductoPresentacion=codigo+presentacion.tipo.substring(0,2).toUpperCase();
                                                    //Introduzco la presentacion del generico
                                                    generico.detalles.push(req.body.detalles);
        
                                                    saveGenerico(generico,req,res,next);
                                                }else{
                                                    res.statusCode=403;
                                                    res.setHeader('Content_Type', 'application/Json');
                                                    res.send("Ingrese una presentacion que exista");
                                                }
                                            
                                            }).catch(e=>next(e))
                                        }else{
                                            res.statusCode=403;
                                            res.setHeader('Content_Type', 'application/Json');
                                            res.send("No puede repetir el mismo codigo de caja");
                                        }

                                    }).catch(e=>next(e))
                                    
                                }else{
                                    Generico.findById(generico._id).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(producto=>{
                                        res.statusCode=200;
                                        res.setHeader('Content_Type', 'application/Json');
                                        res.json(producto); 
                                    }).catch(e=>next(e));
                                }
                            }).catch(e=>next(e));
                        })
                        
                    }).catch(e=>next(e))
                }else{
                    res.statusCode=403;
                    res.setHeader('Content_Type', 'application/Json');
                    res.send("Ese generico ya existe");
                }
            }).catch(e=>next(e));
                
        }else{
            res.statusCode=403;
            res.setHeader('Content_Type', 'application/Json');
            res.send("Ingrese un producto que exista");
        }
    }).catch(e=>next(e));
})

.put(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.json({err:"No puede modificar en este endpoint"});
})

.delete(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.json({err:"No puede eliminar todos los productos"});
})

//GENERICO ID
genericoRouter.route('/:genericoId')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})

.get(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(generico); 
    }).catch(e=>next(e));
})
.post(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.json({err:"No puede postear en este endpoint"});
})
.put(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).then(generico=>{
        if(generico){
            if(req.body.nombreGenerico){
                Generico.findOne({nombreGenerico:req.body.nombreGenerico}).then(generico2=>{
                   
                    if(generico?generico._id.equals(generico2._id):true){
                        Generico.findByIdAndUpdate(req.params.genericoId,{'$set':req.body},{new:true}).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
                            res.statusCode=200;
                            res.setHeader('Content_Type', 'application/Json');
                            res.json(generico); 
                        }).catch(e=>next(e));
                    }else{
                        res.statusCode=403;
                        res.send("Intenta modificar el nombre de este generico con uno ya registrado");
                    }
                
                })
            }else{
                Generico.findByIdAndUpdate(req.params.genericoId,{'$set':req.body},{new:true}).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
                    res.statusCode=200;
                    res.setHeader('Content_Type', 'application/Json');
                    res.json(generico); 
                }).catch(e=>next(e));
            }
            
        }else{
            res.statusCode=403;
            res.send("Ingrese un generico valido");
        }
    }).catch(e=>next(e)); 
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).then(generico=>{
        if(generico){
            Generico.findByIdAndUpdate(req.params.genericoId,{'$set':{activo:false}},{new:true}).populate('producto').populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
                
                generico.detalles.forEach(detalle=>{
                    detalle.activo=false
                })

                saveGenerico(generico,req,res,next);

            }).catch(e=>next(e));
        }else{
            res.statusCode=403;
            res.json({err:"Ingrese un generico valido"});
        }
    }).catch(e=>next(e))
    
})

//DETALLES

genericoRouter.route('/:genericoId/detalles')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})
.get(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(generico.detalles); 
    }).catch(e=>next(e));
})
.post(cors.corsWithOptions,(req,res,next)=>{
    Presentacion.findOne({tipo:req.body.tipo}).then(presentacion=>{
        if(presentacion){
            Generico.findOne({"detalles.codigoCaja":req.body.codigoCaja}).then(generico=>{
                if(!generico){
                    Generico.findById(req.params.genericoId).then(generico=>{
                        req.body.codigoProductoPresentacion=generico.codigoProducto+presentacion.tipo.substring(0,2).toUpperCase();
                        req.body.tipo=presentacion._id
                        generico.detalles.push(req.body)

                        saveGenerico(generico,req,res,next);
        
                    }).catch(e=>next(e));
                }else{
                    res.statusCode=403;
                    res.setHeader('Content_Type', 'application/Json');
                    res.json({err:"El codigo de caja tiene que ser unico"}); 
                }
            }).catch(e=>next(e))
        }else{
            res.statusCode=403;
            res.setHeader('Content_Type', 'application/Json');
            res.json({err:"Ingrese una presentacion que exista"});
        }
    }).catch(e=>next(e));
})
.put(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.json({err:"No puede modificar en este endpoint"});
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).then(generico=>{
        generico.detalle.forEach(detalle=>{
            detalle.activo=false
        })
        saveGenerico(generico,req,res,next);
    }).catch(e=>next(e));
})

genericoRouter.route('/:genericoId/detalles/:detalleId')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})

.get(cors.corsWithOptions, (req,res,next)=>{
    Generico.findById(req.params.genericoId).populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
        var detalle= generico.detalles.id(req.params.detalleId);
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(detalle); 
    }).catch(e=>next(e));
})
.post(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.json({err:"No puede postear en este endpoint"});
})
.put(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).then(generico=>{
        var detalle= generico.detalles.id(req.params.detalleId);
        if(req.body.tipo){
            Presentacion.findOne({tipo:req.body.tipo}).then(presentacion=>{
                if(presentacion){
                    if(req.body.codigoCaja){
                        Generico.findOne({"detalles.codigoCaja":req.body.codigoCaja}).then(generico2=>{
                            if(generico._id.equals(generico2._id)){
                                PutDetalles(generico,detalle,presentacion,req,res,next);
                            }else{
                                res.statusCode=403;
                                res.json({err:"el codigo de caja tiene que ser unico"}); 
                            }
                        }).catch(e=>next(e));
                    }else{
                        //Asignacion de valores nuevos
                        PutDetalles(generico,detalle,presentacion,req,res,next);
                    }
                }else{
                    res.statusCode=403;
                    res.json({err:"Ingrese una presentación valida"});
                }
            }).catch(e=>next(e))
        }else{
            generico.detalles[generico.detalles.indexOf(detalle._id)]=req.body;
            generico.save().then(generico=>{
                Generico.findById(req.params.genericoId).populate('detalles.tipo').populate('detalles.laboratorio').then(generico=>{
                    //Asignacion de valores nuevos
                    PutDetalles(generico,detalle,null,req,res,next);

                }).catch(e=>next(e));
            }).catch(e=>next(e))
        }
    }).catch(e=>next(e))
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Generico.findById(req.params.genericoId).then(generico=>{
        var detalle= generico.detalles.id(req.params.detalleId);
        
        PutDetalles(generico,detalle,null,req,res,next);
    }).catch(e=>next(e));
})
module.exports=genericoRouter;