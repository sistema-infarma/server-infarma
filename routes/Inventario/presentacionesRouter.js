const express= require('express');
const bodyParser= require('body-parser');

const mongoose = require('mongoose');
const Presentacion =require('../../Schemas/presentacion')
const presentacionRouter= express.Router();
const cors= require('../cors');

presentacionRouter.use(bodyParser.json());
presentacionRouter.route('/')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200);})

.get(cors.cors, (req,res,next)=>{
    Presentacion.find(req.query).then(presentaciones=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(presentaciones);
    }).catch(e=>{next(e)})
})

.post(cors.corsWithOptions,(req,res,next)=>{
    Presentacion.create(req.body).then(presentacion=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(presentacion);
    }).catch(e=>{next(e)});
})
.put(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.end('Operation not supported');
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Presentacion.remove({}).then(resp=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(resp);
    })
})

presentacionRouter.route('/:presentacionId')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200);}).options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200);})

.get(cors.cors, (req,res,next)=>{
    Presentacion.findById(req.params.presentacionId).then(presentacion=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(presentacion);
    }).catch(e=>next(e))
})
.post(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.end('Operation not supported');
})

.put(cors.corsWithOptions,(req,res,next)=>{
    Presentacion.findById(req.params.presentacionId,{'$set':req.body},{new:true}).then(presentacion=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(presentacion);
    }).catch(e=>next(e))
})

.delete(cors.corsWithOptions,(req,res,next)=>{
    Presentacion.findByIdAndRemove(req.params.presentacionId).then(resp=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(resp);
    }).catch(e=>next(e))
})
module.exports=presentacionRouter;