const express= require('express');
const bodyParser= require('body-parser');

const mongoose = require('mongoose');

const authenticate= require('../../authenticate');

const Laboratorio = require('../../Schemas/laboratorio')
const cors =require('../cors');

const LaboratorioRouter= express.Router();

LaboratorioRouter.route('/')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})
.get(cors.corsWithOptions,(req,res,next)=>{
    Laboratorio.find(req.query).then(laboratorios=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(laboratorios);
    }).catch(e=>next(e))
})
.post(cors.corsWithOptions,(req,res,next)=>{
    Laboratorio.findOne({nombreLaboratorio:req.body.nombreLaboratorio}).then(laboratorio=>{
        if(!laboratorio){
            Laboratorio.create(req.body).then(laboratorio=>{
                res.statusCode=200;
                res.setHeader('Content_Type', 'application/Json');
                res.json(laboratorio);
            }).catch(e=>next(e))
        }else{
            res.statusCode=403;
            res.setHeader('Content_Type', 'application/Json');
            res.json({err:"Esa Laboratorio ya existe"});
        }
    }).catch(e=>next(e))
})

.put(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.end('No se puede modificar en este endpoint');
})

.delete(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.json({err:"No puede eliminar todas las Laboratorios"});
})

//Laboratorio Especifica
LaboratorioRouter.route('/:laboratorioId')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200)})
.get(cors.corsWithOptions,(req,res,next)=>{
    Laboratorio.findById(req.params.laboratorioId).then(laboratorio=>{
        if(laboratorio){
            res.statusCode=200;
            res.setHeader('Content_Type', 'application/Json');
            res.json(laboratorio);
        }else{
            res.statusCode=403;
            res.json({err:"Ingrese una Laboratorio valida"}); 
        }
    }).catch(e=>next(e))
})
.post(cors.corsWithOptions,(req,res,next)=>{
    res.statusCode=403;
    res.end('No se puede publicar en este endpoint');
})
.put(cors.corsWithOptions,(req,res,next)=>{
    Laboratorio.findById(req.params.laboratorioId).then(laboratorio=>{
        if(laboratorio){
            if(req.paramas.nombrelaboratorio){
                Laboratorio.findOne({nombreLaboratorio:req.body.nombreLaboratorio}).then(laboratorio2=>{
                    if(laboratorio._id.equals(laboratorio2._id)){
                        
                        Laboratorio.findByIdAndUpdate(req.params.laboratorioId,{'$set':req.body},{new:true}).then(laboratorio=>{
                            res.statusCode=200;
                            res.setHeader('Content_Type', 'application/Json');
                            res.json(laboratorio); 
                        }).catch(e=>next(e))

                    }else{
                        res.statusCode=403;
                        res.json({err:"Esta tratando de cambiar el nombre de la Laboratorio por uno ya registrado"}); 
                    }
                }).catch(e=>next(e))
            }else{
                Laboratorio.findByIdAndUpdate(req.paramslaboratorioId,{'$set':req.body},{new:true}).then(laboratorio=>{
                    res.statusCode=200;
                    res.setHeader('Content_Type', 'application/Json');
                    res.json(laboratorio); 
                }).catch(e=>next(e))
            }
        }else{
            res.statusCode=403;
            res.json({err:"Ingrese una Laboratorio valida"}); 
        }
    }).catch(e=>next(e))
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Laboratorio.findByIdAndUpdate(req.params.laboratorioId,{'$set':{activo:false}},{new:true}).then(laboratorio=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(laboratorio); 
    }).next(e=>next(e))
})

module.exports=LaboratorioRouter;