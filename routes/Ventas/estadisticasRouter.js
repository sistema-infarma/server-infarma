const express= require('express');
const bodyParser= require('body-parser');

const mongoose = require('mongoose');
const Ventas =require('../../Schemas/ventas')
const Producto= require('../../Schemas/producto');
const estadisticasRouter= express.Router();
const cors= require('../cors');
const Presentacion = require('../../Schemas/presentacion');



estadisticasRouter.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200);})
estadisticasRouter.get('/total',cors.corsWithOptions,(req,res,next)=>{
    Ventas.aggregate([
        {$match:{}},
        {$group:{_id:null,total:{$sum:"$total"},numerodeventas:{$sum:1}}}
    ],(err,resp)=>{
        if(!err){
            res.statusCode=200;
            res.setHeader('Content_Type', 'application/Json');
            res.json(resp);
        }else{
            console.log(err);
        }
    })
})

estadisticasRouter.get('/totalxtrabajador',cors.corsWithOptions,(req,res,next)=>{
    Ventas.aggregate([
        {$match:{}},
        {$group:{_id:"$trabajador",total:{$sum:"$total"},numerodeventas:{$sum:1}}}
    ],(err,resp)=>{
        if(!err){
            res.statusCode=200;
            res.setHeader('Content_Type', 'application/Json');
            res.json(resp);
        }else{
            console.log(err);
        }
    })
})

estadisticasRouter.get('/totalxproducto',cors.corsWithOptions,(req,res,next)=>{
    Ventas.aggregate([
        {$match:{}},
        {$unwind:"$ordenes"},
        {$group:{_id:"$ordenes.producto",total:{$sum:"$ordenes.subtotal"},numerodeventas:{$sum:"$ordenes.cantidad"}}},
    ],(err,resp)=>{
        if(!err){
          resp= resp.map(calculo=>{
               return Producto.findById(calculo._id).then(producto=>{
                   calculo._id=producto.nombre
                   return calculo
                })
            })
            Promise.all(resp).then(resp=>{
                res.statusCode=200;
                res.setHeader('Content_Type', 'application/Json');
                res.json(resp);
            })
        }else{
            console.log(err);
        }
    })
})

estadisticasRouter.get('/mejorproducto',cors.corsWithOptions,(req,res,next)=>{
    Ventas.aggregate([
        {$match:{}},
        {$unwind:"$ordenes"},
        {$group:{_id:"$ordenes.producto",total:{$sum:"$ordenes.subtotal"},numerodeventas:{$sum:"$ordenes.cantidad"}}},
        { $sort: { numeroventas : -1 } },
        { $limit: 1 } 
    ],(err,resp)=>{
        if(!err){
          resp= resp.map(calculo=>{
               return Producto.findById(calculo._id).then(producto=>{
                   calculo._id=producto.nombre
                   return calculo
                })
            })
            Promise.all(resp).then(resp=>{
                res.statusCode=200;
                res.setHeader('Content_Type', 'application/Json');
                res.json(resp);
            })
        }else{
            console.log(err);
        }
    })
})

estadisticasRouter.get('/mayorvendedor',cors.corsWithOptions,(req,res,next)=>{
    Ventas.aggregate([
        {$match:{}},
        {$group:{_id:"$trabajador",total:{$sum:"$total"},numerodeventas:{$sum:1}}},
        { $sort:{numerodeventas: -1}},
        { $limit: 1 } 
    ],(err,resp)=>{
        if(!err){
          resp= resp.map(calculo=>{
               return Producto.findById(calculo._id).then(producto=>{
                   calculo._id=producto.nombre
                   return calculo
                })
            })
            Promise.all(resp).then(resp=>{
                res.statusCode=200;
                res.setHeader('Content_Type', 'application/Json');
                res.json(resp);
            })
        }else{
            console.log(err);
        }
    })
})

module.exports=estadisticasRouter;