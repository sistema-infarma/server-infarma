const express= require('express');
const bodyParser= require('body-parser');

const mongoose = require('mongoose');
const Ventas =require('../../Schemas/ventas')
const Producto= require('../../Schemas/producto');
const ventasRouter= express.Router();
const cors= require('../cors');
const Presentacion = require('../../Schemas/presentacion');
const estadisticasRouter = require('./estadisticasRouter')

ventasRouter.use(bodyParser.json());
ventasRouter.use('/estadisticas',estadisticasRouter);
ventasRouter.route('/')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200);})

.get(cors.corsWithOptions,(req,res,next)=>{
    Ventas.find(req.query).then(ventas=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(ventas);
    }).catch(e=>next(e));
})

.post(cors.corsWithOptions,(req,res,next)=>{

    Ventas.create(req.body).then(venta=>{
            ordenes=venta.ordenes.map(orden=>{

                return Producto.findById(orden.producto).then(producto=>{
                    console.log(producto)
                    return Presentacion.findOne({tipo:orden.tipo}).then(presentacion=>{
                        index=producto.presentacion.map(p=>p.presentacion).indexOf(presentacion._id);
                        if(index!== -1){
                            if(orden.cantidad<producto.presentacion[index].cantidad){
                                orden.subtotal=producto.presentacion[index].precio*orden.cantidad
                                return orden;
                            }else{
                                res.statusCode = 403;
                                res.end('No puede comprar mas de la cantidad disponible en el inventario ');
                            }
                        }
                    })
                })
            })
            Promise.all(ordenes).then((ordenes)=>{
                
                venta.ordenes=ordenes;
                venta.save().then(venta=>{

                    var total=Ventas.aggregate([
                        {$match:{_id:mongoose.Types.ObjectId(venta._id)}},
                        {$unwind:"$ordenes"},
                        {$group:{_id:null,total:{$sum:"$ordenes.subtotal"}}},
                        {$project:{total:1}}
                    ],(err,result)=>{
                        if(!err){
                                venta.total=result[0].total;
                                venta.save().then(venta=>{
                                res.statusCode=200;
                                res.setHeader('Content_Type', 'application/Json');
                                res.json(venta);
                            })   
                        }
                    });
                });
            })
    })
})

.put(cors.corsWithOptions,(req,res,next)=>{
    err= new Error('Operation not suported');
    err.status=403;
    return next(err);
})

.delete(cors.corsWithOptions,(req,res,next)=>{
    Ventas.deleteMany({}).then(resp=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(resp);
    })
})

ventasRouter.route('/:ventaId')
.options(cors.corsWithOptions,(req,res)=>{res.sendStatus(200);})

.get(cors.corsWithOptions,(req,res,next)=>{
    Ventas.findById(req.params.ventaId).then(venta=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(venta);
    }).catch(e=>next(e));
})
.post(cors.corsWithOptions,(req,res,next)=>{
    err= new Error('Operation not suported');
    err.status=403;
    return next(err);
})
.put(cors.corsWithOptions,(req,res,next)=>{
    err= new Error('Operation not suported');
    err.status=403;
    return next(err);
})
.delete(cors.corsWithOptions,(req,res,next)=>{
    Ventas.findByIdAndDelete(req.params.ventaId).then(resp=>{
        res.statusCode=200;
        res.setHeader('Content_Type', 'application/Json');
        res.json(resp);
    }).catch(e=>next(e))
})


module.exports=ventasRouter;