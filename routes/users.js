var express = require('express');
var router = express.Router();
const cors= require('./cors');
const User =require('../Schemas/usuario');
const bodyParser = require('body-parser');
const passport = require('passport');
const authenticate = require('../authenticate');
/* GET users listing. */
router.use(bodyParser.json());


router.options('*',cors.corsWithOptions,(req,res)=>{res.sendStatus(200);})
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/signup',cors.corsWithOptions,(req,res,next)=>{
    User.register(new User({username: req.body.username}),req.body.password, (err,user)=>{
      if(err){
        res.statusCode = 500;
        res.setHeader('Content-Type', 'application/json');
        res.json({ err: err });
      }else{
        if (req.body.firstname) {
          user.firstname = req.body.firstname;
        }
        if (req.body.lastname) {
            user.lastname = req.body.lastname;
        }

        user.save().then((user,err) => {
            if (err) {
                res.statusCode = 500;
                res.setHeader('Content-Type', 'application/json');
                res.json({ err: err });
            } else {
                passport.authenticate('local')(req, res, () => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json({ succes: true, status: 'Registration successfull' });
                });
            }
        });
      }
    })
});

router.post('/login',cors.corsWithOptions,(req,res,next)=>{
    passport.authenticate('local',(err,user,info)=>{
      if(err){
        return next(err);
      }else if(!user){
        res.statusCode = 401;
            res.setHeader('Content-Type', 'application/json');
            res.json({ succes: false, status: 'Login unsuccessfull', err: info });
      }

      req.logIn(user,(err)=>{
        if(err){
          res.statusCode = 401;
          res.setHeader('Content-Type', 'application/json');
          res.json({ succes: false, status: 'Login unsuccessfull', err: info });
        }
        const token =authenticate.getToken({_id:req.user._id});
        res.statusCode=200;
        res.setHeader('Content-Type', 'application/json');
        res.json({ succes: true, token: token, admin:user.admin, status: 'Login successfull' });
      })
    })(req, res, next);
})

router.get('/logout', cors.corsWithOptions, (req, res, next) => {
  if (req.session) {
      req.session.destroy();
      res.clearCookie('session-id');
      res.redirect('/');
  } else {
      const err = new Error(`You are not logged in`);
      err.status = 403;
      return next(err);
  }
});

router.post('/changepassword',cors.corsWithOptions,(req,res,next)=>{
    passport.authenticate('local',(err,user,info)=>{
        user.changePassword(req.body.oldPassword, req.body.newPassword, function (err, user) {
            if (err) next(err);
            res.json('password changes successfully !');
        })
    })(req, res, next);
})


router.get('/checkJWTToken', cors.corsWithOptions, (req, res) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err)
          return next(err)
      if (!user) {
          res.statusCode = 401;
          res.setHeader('Content-Type', 'application/json');
          res.json({ status: 'JWT invalid', succes: false, err: info })
      } else {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({ succes: true, status: 'JWT valid', user: user });
      }
  })(req, res);
})





module.exports = router;
