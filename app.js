var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var config = require('./config');


var passport= require('passport')
var mongoose= require('mongoose');

//ROUTES
const ventasRouter=require('./routes/Ventas/ventasRouter')
const productoRouter=require('./routes/Inventario/productoRouter');
const presentacionesRouter=require('./routes/Inventario/presentacionesRouter');
const laboratoriosRouter=require('./routes/Inventario/LaboratoriosRouter');
const url = config.mongoUrl;

const connect = mongoose.connect(url,{
  useCreateIndex:true,
  useNewUrlParser:true,
  useUnifiedTopology:true,
  useFindAndModify:false
});

connect.then(db=>{
  console.log('Connected corretly to server');
}).catch(e=>{console.log(e)});

var app = express();

app.all('*',(req,res,next)=>{
  if(req.secure){
    return next();
  }else{
    res.redirect(307, `https://${req.hostname}:${app.get('secure')}`);
  }
})



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));




app.use(passport.initialize());
app.use('/users', usersRouter);
app.use(express.static(path.join(__dirname, 'public')));

app.use('/ventas',ventasRouter)
app.use('/presentaciones',presentacionesRouter);
app.use('/productos',productoRouter);
app.use('/laboratorios',laboratoriosRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
